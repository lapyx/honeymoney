var help = new Vue({
    el: '#help',
    data: {
        visible: true
    }
});

var article = new Vue({
    el: '#article',
    data: {
        // последний пользователь сделавший ставку
        lastUser: {
            name: 'Laptev5994', // логин
            sum: 10 // ставка
        },
        // общая накопленная сумма ставок
        jackpot: 1400,
        // таймер в секундах
        lefttime: 299,
        intvalId: null
    },
    methods : {
        timeleft: function() {
            var d = new Date(0,0,0,0,0,this.lefttime);
            var m = d.getMinutes().toString();
            var s = d.getSeconds().toString();
            m = m < 10 ? '0'+m : m;
            s = s < 10 ? '0'+s : s;
            return m+':'+s;
        },
        setBet: function () {
            var bet = prompt('Сколько хотите сделать ставку?');
            if (bet) {
                this.jackpot += Number(bet) | 0;
                this.lastUser = {
                    name: login.name,
                    sum: bet
                };
                alert('Ставка сделана');
            }
        },
        startTimer: function () {
            if(this.intvalId) {
                clearInterval(this.intvalId);
            }
            var self = this;
            this.intvalId = setInterval(function () {
                if (self.lefttime)
                    self.lefttime--;

                if (self.lefttime <= 0) {
                    alert('Время истекло');
                    if(self.intvalId) {
                        clearInterval(self.intvalId);
                    }
                }
            },1000);
        }
    },
    created: function () {
        this.startTimer();
    }
});

var chat = new Vue({
    el: '#chat',
    data: {
        visible: true,
        msg: '',
        messages: []
    },
    methods: {
        sendMsg: function () {
            var oneMsg = {
                text: this.msg,
                name: this.name,
                time: new Date()
            };
            this.messages.push(oneMsg);
            alert(this.msg+"\n\nСообщение отправлено!");
            this.msg = '';
        }
    }
});

var login = new Vue({
    el: '#login',
    data: {
        name: 'Laptev5994',
        phone: '9872600085'
    },
    methods: {
        getPhoneFormat: function () {
            return this.phone.replace(/^(?:\+?(?:7|8))?(\d{3})(\d{3})(\d{2})(\d{2})$/,"7 $1 $2 $3 $4");
        }
    }
});